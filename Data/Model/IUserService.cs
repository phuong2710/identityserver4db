﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public interface IUserService
    {
         void Create(User user);
         void Update(User user);
         void Delete(int userId);
      
    }
}

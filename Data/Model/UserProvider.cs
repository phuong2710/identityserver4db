﻿using Dapper;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class UserProvider : IUserProvider
    {
        private string connectionString;

        public UserProvider(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public IEnumerable<User> Get()
        {
            IEnumerable<User> user = null;
            using (var conn = new MySqlConnection(connectionString))
            {
                user = conn.Query<User>("select UserId,Username from T_User");
            }

            return user;
        }
    }
}

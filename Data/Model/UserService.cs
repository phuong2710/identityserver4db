﻿using Dapper;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class UserService : IUserService
    {
        private string connectionString;
        public UserService(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public void Create(User user)
        {
            using(var connection = new MySqlConnection(connectionString))
            {
                string sql = "insert into T_User(Username,Password) values(@Username, @Password)";
                //var hash = new PasswordHash(user.Password);
                //byte[] hashBytes = hash.ToArray();
                connection.Execute(sql, new { user.Username, user.Password });
            }
        }

        public void Delete(int userId)
        {
            using (var connection = new MySqlConnection(connectionString))
            {
                string sql = "delete from T_User where UserId = @Id";
                //var hash = new PasswordHash(user.Password);
                //byte[] hashBytes = hash.ToArray();
                connection.Execute(sql, new { Id = userId });
            }
        }

        public void Update(User user)
        {
            throw new NotImplementedException();
        }
    }
}
